#!/usr/bin/env node

const ytdl = require("../lib/ytdl-cli");

const filter = process.argv[3] || "video";
const url = process.argv[2];

if (!url) {
    console.log("Usage: node app.js <url> [filter]");
    return 1;
}

try {
    ytdl(url, filter);
} catch (err) {
    console.log("Error while attempting download: ", err);
}

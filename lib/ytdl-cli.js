const fs = require("fs");
const ytdl = require("ytdl-core");

// TODO: support path
// TODO: show progress
// TODO: add -h, --help
module.exports = (url, filter) => {
    ytdl.getInfo(url, (err, info) => {
        if (err) {
            console.log("Error returned: ", err);
            return 1;
        }
        let title = info.title.replace(/ /g, "_");
        if (filter === "audioonly") {
            title += ".mp3";
        } else {
            title += ".mp4";
        }
        ytdl.downloadFromInfo(info, { filter }).pipe(
            fs.createWriteStream(title)
        );
    });
};
